using System.Collections;
using System.Collections.Generic;
using TensorFlowLite;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
using TMPro;
public class BodyAnimate : MonoBehaviour
{
    public GameObject[] Rhand;
    public GameObject[] Lhand;
    public GameObject[] Rleg;
    public GameObject[] Lleg;
    public GameObject upperbody;
    public GameObject[] Plams;

    public Text text;
    public GameObject temp;
    public static BodyAnimate instance;

    public GameObject body;
    public float bodyoffset;
    public Vector3 bodyoffsetvec;
    public float bodyfacter;
    public float facter;
    bool t=true;
    Vector3 root;
    public Vector3 offset;
    public Quaternion BDoffset;
    public Vector3 offsetpos;
    public float angleOffset;
    [Header("Parent")]
    public GameObject[] RhandParent;
    public GameObject[] LhandParent;
    public GameObject[] RlegParent;
    public GameObject[] LlegParent;
    public GameObject upperbodyParent;
    public GameObject[] PlamsParent;
    public GameObject bodyParent;
    List<GameObject> temobj=new List<GameObject>();

    public float speed = 5;
    public TMP_Text speedtext;
    public Slider mainSlider;
    public float currentDistance;
    private Vector3 Open;
    public Vector3 Close;

    Vector3 camPos,startCamPos;
    Quaternion camRotation;
    float camRotY;
    public bool iscamposset;
    public float[] betweenAngle;

    public GameObject landmarkParent;
    void Start()
    {
        
        body.transform.position = Camera.main.transform.position + Camera.main.transform.forward * bodyoffset+ bodyoffsetvec;
        instance = this;
        mainSlider.onValueChanged.AddListener(delegate { setSpeed(); });

       
    }
    void setStartPos(GameObject t, Transform parent) {
      //  t.transform.position = parent.position;
       // t.transform.rotation = Quaternion.identity;
    }
    public void setSpeed() {
        angleOffset = mainSlider.value;
        speedtext.text = angleOffset.ToString();
    }
    void set(GameObject t, Vector4 p, float visibilityThreshold,Transform parent) {
      
        Open = t.transform.position;
        Close = parent.position + (Vector3)p * facter + offsetpos;
        currentDistance = Vector3.Distance(Open, Close);
        float Perc = currentDistance / speed;
        t.transform.position = Vector3.Lerp(Open, Close, Perc);
       // parent.rotation = Quaternion.LookRotation(p,parent.right);

            }
    public void Animate(PoseLandmarkDetect.Result result, float visibilityThreshold) {
      
        Vector4[] landmarks = result.viewportLandmarks;
        float tempangle =Quaternion.FromToRotation((landmarks[0] - landmarks[1]), Vector3.forward).y*Mathf.Rad2Deg;
        float angle;
        Vector4 RH1;
        Vector4 RH2;
        Vector4 RL1;
        Vector4 RL2;
        Vector4 LH1;
        Vector4 LH2;
        Vector4 LL1;
        Vector4 LL2,RP,LP,BD;
        camRotation = Quaternion.Euler(0, camRotY,0);

      //  Debug.Log(Quaternion.FromToRotation(landmarks[23]-landmarks[24],Camera.main.transform.forward).eulerAngles);
      //  body.transform.rotation = camRotation* Quaternion.FromToRotation(landmarks[23] - landmarks[24], Camera.main.transform.forward);
        
        Open = body.transform.position;
        Close = camPos + camRotation * bodyoffsetvec + camRotation * (Vector3)((landmarks[23] + landmarks[24]) / 2* bodyfacter);
        currentDistance = Vector3.Distance(Open, Close);
        float Percb = currentDistance / speed;
        transform.position = Vector3.Lerp(Open, Close, Percb);
        Quaternion quaternion= Quaternion.Euler(0, 0, 0); ;
        RH1 = quaternion * (landmarks[13] - landmarks[11]);
        RH2 = quaternion * (landmarks[15] - landmarks[13]);
        RL1 = quaternion * (landmarks[25] - landmarks[23]);
        RL2 = quaternion * (landmarks[27] - landmarks[25]);
        LH1 = quaternion * (landmarks[14] - landmarks[12]);
        LH2 = quaternion * (landmarks[16] - landmarks[14]);
        LL1 = quaternion * (landmarks[26] - landmarks[24]);
        LL2 = quaternion * (landmarks[28] - landmarks[26]);

        RP= quaternion * (landmarks[17] - landmarks[15]);
        LP = quaternion * (landmarks[18] - landmarks[16]);

        BD= Vector3.Cross((landmarks[23] - landmarks[24]), Vector3.up); 
        // Rhand[0].transform.localRotation = Quaternion.FromToRotation(landmarks[11] ,landmarks[13]) * Quaternion.Euler(0, angleOffset, 0);
        // Rhand[1].transform.localRotation = Quaternion.LookRotation(landmarks[13] - landmarks[11]) * Quaternion.Euler(0, angleOffset, 0);

        root = (Vector3) ((landmarks[23] + landmarks[24]) / 2 * facter)-body.transform.position;
        set(Rhand[0], RH1, visibilityThreshold, RhandParent[0].transform);
        set(Rhand[1], RH2, visibilityThreshold, RhandParent[1].transform);

        set(Lhand[0], LH1, visibilityThreshold, LhandParent[0].transform);
        set(Lhand[1], LH2, visibilityThreshold, LhandParent[1].transform);

        set(Rleg[0], RL1, visibilityThreshold, RlegParent[0].transform);
        set(Rleg[1], RL2, visibilityThreshold, RlegParent[1].transform);

        set(Lleg[0], LL1, visibilityThreshold, LlegParent[0].transform);
        set(Lleg[1], LL2, visibilityThreshold, LlegParent[1].transform);

        set(PlamsParent[0], RP, visibilityThreshold, Plams[0].transform);
        set(PlamsParent[1], LP, visibilityThreshold, Plams[1].transform);


        //set(body.transform.parent.gameObject, BD, visibilityThreshold, body.transform);

        Open = body.transform.position;
        Close =(bodyParent.transform.position + camRotation  * (Vector3)(BD * facter) + offsetpos);
        currentDistance = Vector3.Distance(Open, Close);
        float Perc = currentDistance / speed;
        body.transform.position = Vector3.Lerp(Open,  Close, Perc);
       

        Open = upperbody.transform.position;
        Close = upperbodyParent.transform.position + (Vector3)( (landmarks[11] + landmarks[12]) / 2 * facter) + offsetpos + offset;
        currentDistance = Vector3.Distance(Open,Close);
        Perc = currentDistance / speed;
        upperbody.transform.position = Vector3.Lerp(Open, Close, Perc);
        for (int i = 0; i < landmarks.Length; i++)
        {

            Vector4 p = landmarks[i];
            if (t)
            {
                GameObject g = Instantiate(temp, landmarkParent.transform);
                g.transform.localPosition=p;
                g.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = i.ToString();
                temobj.Add(g);
            }
            if (p.w > visibilityThreshold)
            {

                //s += $"({i},{p}),";
                if (temobj[i] == null)
                {
                    GameObject g = Instantiate(temp, p, Quaternion.identity);
                    g.transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = i.ToString();
                    temobj.Add(g);
                }
                else
                {
                    temobj[i].transform.position = p;
                    temobj[i].transform.GetChild(0).GetComponentInChildren<TMP_Text>().text = i.ToString();
                }
                // draw.Cube(p, 0.02f);
            }
        }
        t = false;
        //  text.text = s;
        var connections = PoseLandmarkDetect.Connections;
        for (int i = 0; i < connections.Length; i += 2)
        {
            var a = landmarks[connections[i]];
            var b = landmarks[connections[i + 1]];
            if (a.w > visibilityThreshold || b.w > visibilityThreshold)
            {
               // draw.Line3D(a, b, 0.005f);
            }
        }

        //draw.Apply();
    }

    public void Disable(bool t) {
        if (t && !iscamposset)
        {
          //  transform.rotation = Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y+angleOffset, 0);
          
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * bodyoffset + bodyoffsetvec;
            camPos = Camera.main.transform.position + Camera.main.transform.forward * bodyoffset;
            startCamPos = transform.position- Camera.main.transform.position;
            startCamPos = new Vector3(0,0, 0);
            camRotY = Camera.main.transform.rotation.eulerAngles.y;
            camRotation = Camera.main.transform.rotation;
            
            iscamposset = true;
        }
        if(!t) {
            iscamposset = false;
        }
        gameObject.SetActive(t);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
