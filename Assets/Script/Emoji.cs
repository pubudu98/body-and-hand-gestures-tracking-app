using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Emoji", menuName = "ScriptableObjects/Emoji", order = 1)]
public class Emoji : ScriptableObject
{
    public string Emojiname;
    public Texture sign;
}
