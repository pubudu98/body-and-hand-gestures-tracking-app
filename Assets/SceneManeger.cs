using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class SceneManeger : MonoBehaviour
{
    public GameObject ar;
    public void LoadScene(int i) {
        if (ar != null)
        {
           // Destroy(ar);
        }
        if (i == 0)
        {
            // 
            LoaderUtility.Deinitialize();
            SceneManager.LoadScene(i, LoadSceneMode.Single);
            
        }
        else {
            
            SceneManager.LoadScene(i, LoadSceneMode.Single);
            LoaderUtility.Initialize();
        }

    }
}
