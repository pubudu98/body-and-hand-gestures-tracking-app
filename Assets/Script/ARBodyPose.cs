using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using TensorFlowLite;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;



public class ARBodyPose : MonoBehaviour
{
    [SerializeField]
    private BlazePose.Options options = default;

   // [SerializeField]
   // private RectTransform containerView = null;
    [SerializeField]
    private RawImage debugView = null;
   // [SerializeField]
   // private Canvas canvas = null;
    [SerializeField]
    private bool runBackground;
    [SerializeField, Range(0f, 1f)]
    private float visibilityThreshold = 0.5f;


    private BlazePose pose;
    private PoseDetect.Result poseResult;
    private PoseLandmarkDetect.Result landmarkResult;
    private BlazePoseDrawer drawer;

    private UniTask<bool> task;
    private CancellationToken cancellationToken;

    [SerializeField] Texture tt;
    [SerializeField] SpriteRenderer test;
    public bool istest;
    public bool isShow;
    [SerializeField] private ARCameraManager cameraManager;
   // [SerializeField] private Emoji[] emojiLib;
    Gesture[] knownGestures;
    GestureEstimator estimator;
    public float offset;
   // [SerializeField] RawImage tex;
    void OnEnable()
    {
        
        if (cameraManager == null) { cameraManager = FindObjectOfType<ARCameraManager>(); }
        cameraManager.frameReceived += OnCameraFrameReceived;
    }

    private void OnCameraFrameReceived(ARCameraFrameEventArgs obj)
    {

        // Get information about the device camera image.
        if (cameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
        {
            // If successful, launch a coroutine that waits for the image
            // to be ready, then apply it to a texture.
            image.ConvertAsync(new XRCpuImage.ConversionParams
            {
                // Get the full image.
                inputRect = new RectInt(0, 0, image.width, image.height),

                // Downsample by 2.
                outputDimensions = new Vector2Int(image.width / 4, image.height / 4),

                // Color image format.
                outputFormat = TextureFormat.RGB24,

                // Flip across the Y axis.
               // transformation = XRCpuImage.Transformation.MirrorY

                // Call ProcessImage when the async operation completes.
            }, ProcessImage);

            // It's safe to dispose the image before the async operation completes.
            image.Dispose();
        }

    }
    void ProcessImage(XRCpuImage.AsyncConversionStatus status, XRCpuImage.ConversionParams conversionParams, NativeArray<byte> data)
    {
        if (status != XRCpuImage.AsyncConversionStatus.Ready)
        {
            Debug.LogErrorFormat("Async request failed with status {0}", status);
            return;
        }
       
        // Do something useful, like copy to a Texture2D or pass to a computer vision algorithm.
        Texture2D tex2 =TempObjectManager.CreateTexture2D(
             conversionParams.outputDimensions.x,
             conversionParams.outputDimensions.y,
             conversionParams.outputFormat,
             false);
       
        tex2.LoadRawTextureData(data);
        tex2.Apply();
        // Do something useful, like copy to a Texture2D or pass to a computer vision algorithm.
        OnTextureUpdate(rotateTexture(tex2,true));
       

        //OnTextureUpdate(tex2);
        // Data is destroyed upon return; no need to dispose.
    }
    Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = TempObjectManager.CreateTexture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
      
        return rotatedTexture;
    }

    public static Texture2D FlipTexture( Texture2D original)
    {
        int textureWidth = original.width;
        int textureHeight = original.height;

        UnityEngine.Color[] colorArray = original.GetPixels();

        for (int j = 0; j < textureHeight; j++)
        {
            int rowStart = 0;
            int rowEnd = textureWidth - 1;

            while (rowStart < rowEnd)
            {
                UnityEngine.Color hold = colorArray[(j * textureWidth) + (rowStart)];
                colorArray[(j * textureWidth) + (rowStart)] = colorArray[(j * textureWidth) + (rowEnd)];
                colorArray[(j * textureWidth) + (rowEnd)] = hold;
                rowStart++;
                rowEnd--;
            }
        }

        Texture2D finalFlippedTexture = TempObjectManager.CreateTexture2D(original.width, original.height);
        finalFlippedTexture.SetPixels(colorArray);
        finalFlippedTexture.Apply();
     
        return finalFlippedTexture;
    }
   
    void OnDisable()
    {
        cameraManager.frameReceived -= OnCameraFrameReceived;
    }
    // Start is called before the first frame update
    private void Start()
    {
        if (cameraManager == null) { cameraManager = FindObjectOfType<ARCameraManager>(); }
        pose = new BlazePose(options);

       // drawer = new BlazePoseDrawer(Camera.main, gameObject.layer, containerView);

        cancellationToken = this.GetCancellationTokenOnDestroy();

        StartCoroutine(enumerator());
    }

    private void OnDestroy()
    {
        pose?.Dispose();
        drawer?.Dispose();
    }
    IEnumerator enumerator() {
        while (istest) {
            OnTextureUpdate(test.sprite.texture);
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForSeconds(1f);
    }
    private void Update()
    {
        
       // drawer.DrawPoseResult(poseResult);
        //Debug.Log(landmarkResult != null? landmarkResult.score.ToString():"hi");
        if (landmarkResult != null && landmarkResult.score > 0.2f)
        {
            BodyAnimate.instance.Disable(true);
           // drawer.DrawCropMatrix(pose.CropMatrix);
            BodyAnimate.instance.Animate(landmarkResult, visibilityThreshold);
            //if (isShow)
            //{
            //    // drawer.DrawLandmarkResult(landmarkResult, visibilityThreshold, canvas.planeDistance);
            //    if (options.landmark.useWorldLandmarks)
            //    {
            //      //  drawer.DrawWorldLandmarks(landmarkResult, visibilityThreshold);
            //    }
            //}
            // 

        }
        else {
            BodyAnimate.instance.Disable(false);
        }
       
    }

    private void OnTextureUpdate(Texture texture)
    {
        try
        {
          //  texture = FlipTexture((Texture2D)texture);
          
           // Debug.Log(Camera.main.transform.rotation.eulerAngles.z+ offset);
            texture = rotateATexture((Texture2D)texture, Camera.main.transform.rotation.eulerAngles.z+ offset);
          //  debugView.texture = rotateATexture((Texture2D)test.sprite.texture, Camera.main.transform.rotation.eulerAngles.z + offset); ;
          //  debugView.SetNativeSize();
            if (runBackground)
            {
                //if (task.Status.IsCompleted())
                //{
                //    task = InvokeAsync(texture);
                //}
            }
            else
            {
                Invoke(texture);
            }
        }
        catch
        {
            Debug.LogError("error");
        }
        TempObjectManager.Clear();
    }

    private void Invoke(Texture texture)
    {
        landmarkResult = pose.Invoke(texture);
        poseResult = pose.PoseResult;
            Destroy(texture);
        //if (pose.LandmarkInputTexture != null)
        //{
        // //   debugView.texture = pose.LandmarkInputTexture;
        //}

    }

    private async UniTask<bool> InvokeAsync(Texture texture)
    {
        landmarkResult = await pose.InvokeAsync(texture, cancellationToken);
        poseResult = pose.PoseResult;
        if (pose.LandmarkInputTexture != null)
        {
           // debugView.texture = pose.LandmarkInputTexture;
        }
        return landmarkResult != null;
    }

    static Texture2D rotateATexture(Texture2D tex, float angle)
    {
        
        Texture2D rotImage =TempObjectManager.CreateTexture2D(tex.width, tex.height);
        int x, y;
        float x1, y1, x2, y2;

        int w = tex.width;
        int h = tex.height;
        float x0 = rot_x(angle, -w / 2.0f, -h / 2.0f) + w / 2.0f;
        float y0 = rot_y(angle, -w / 2.0f, -h / 2.0f) + h / 2.0f;

        float dx_x = rot_x(angle, 1.0f, 0.0f);
        float dx_y = rot_y(angle, 1.0f, 0.0f);
        float dy_x = rot_x(angle, 0.0f, 1.0f);
        float dy_y = rot_y(angle, 0.0f, 1.0f);


        x1 = x0;
        y1 = y0;

        for (x = 0; x < tex.width; x++)
        {
            x2 = x1;
            y2 = y1;
            for (y = 0; y < tex.height; y++)
            {
                //rotImage.SetPixel (x1, y1, Color.clear);          

                x2 += dx_x;//rot_x(angle, x1, y1);
                y2 += dx_y;//rot_y(angle, x1, y1);
                rotImage.SetPixel((int)Mathf.Floor(x), (int)Mathf.Floor(y), getPixel(tex, x2, y2));
            }

            x1 += dy_x;
            y1 += dy_y;

        }

        rotImage.Apply();
        return rotImage;
    }

    private static Color getPixel(Texture2D tex, float x, float y)
    {
        Color pix;
        int x1 = (int)Mathf.Floor(x);
        int y1 = (int)Mathf.Floor(y);

        if (x1 > tex.width || x1 < 0 ||
           y1 > tex.height || y1 < 0)
        {
            pix = Color.clear;
        }
        else
        {
            pix = tex.GetPixel(x1, y1);
        }

        return pix;
    }

    private static float rot_x(float angle, float x, float y)
    {
        float cos = Mathf.Cos(angle / 180.0f * Mathf.PI);
        float sin = Mathf.Sin(angle / 180.0f * Mathf.PI);
        return (x * cos + y * (-sin));
    }
    private static float rot_y(float angle, float x, float y)
    {
        float cos = Mathf.Cos(angle / 180.0f * Mathf.PI);
        float sin = Mathf.Sin(angle / 180.0f * Mathf.PI);
        return (x * sin + y * cos);
    }

}
public static class TempObjectManager
{

    static List<UnityEngine.Object> tempObjects =new List<UnityEngine.Object>();

    static  TempObjectManager()
    {
       // tempObjects = new List<Object>();
    }

    /// <summary>
    /// Call this from some MonoBehaviour in OnDisable to destroy all objects.
    /// </summary>
    public static void OnDisable()
    {
        Clear();
    }

    public static void Clear()
    {
        // Destroy all temp objects in the manager
        for (int i = 0; i < tempObjects.Count; i++)
        {
            if (tempObjects[i] == null) continue;
            UnityEngine.Object.Destroy(tempObjects[i]);
        }
        tempObjects.Clear(); // clear the list
    }

    /// <summary>
    /// Adds a temp object to the manager.
    /// </summary>
    /// <param name="obj">Texture</param>
    public static void Add(UnityEngine.Object obj)
    {
        if (obj == null) return;
        if (tempObjects.Contains(obj)) return; // already in the list
        tempObjects.Add(obj); // add to list
    }

    /// <summary>
    /// Destroys the object and removes it from the manager.
    /// </summary>
    /// <param name="obj">Object</param>
    public static void Destroy(UnityEngine.Object obj)
    {
        if (obj == null) return;
        tempObjects.Remove(obj); // remove from list
        UnityEngine.Object.Destroy(obj); // destroy the object
    }

    /// <summary>
    /// Creates a temporary texture and stores it in the manager.
    /// </summary>
    /// <param name="width">Width of the texture</param>
    /// <param name="height">Height of the texture</param>
    /// <returns>Texture2D</returns>
    public static Texture2D CreateTexture2D(int width, int height)
    {
        Texture2D tex = new Texture2D(width, height);
        tex.hideFlags = HideFlags.HideAndDontSave;
        tempObjects.Add(tex);
        return tex;
    }
    public static Texture2D CreateTexture2D(int width, int height, TextureFormat textureFormat, bool mipChain)
    {
        Texture2D tex = new Texture2D(width, height, textureFormat, mipChain);
        tex.hideFlags = HideFlags.HideAndDontSave;
        tempObjects.Add(tex);
        return tex;
    }

    
}