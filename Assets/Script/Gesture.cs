using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using TensorFlowLite;
using UnityEngine;
public class Finger
{
    public static List<int> all = new List<int> { 0, 1, 2, 3, 4 };
    public static int Thumb = 0;
    public static int Index = 1;
    public static int Middle = 2;
    public static int Ring = 3;
    public static int Pinky = 4;

    static Dictionary<string, int> names = new Dictionary<string, int>(){
        {"Thumb",  0 },
        {"Index",  1 },
        {"Middle",  2 },
        {"Ring",  3 },
        {"Pinky",  4 },
    };

    static Dictionary<int, string> nameMapping = new Dictionary<int, string>(){
        {0,"Thumb" },
        {1,"Index"},
        {2,"Middle" },
        {3,"Ring" },
        {4,"Pinky" },
    };

    static Dictionary<int, List<List<int>>> pointsMapping = new Dictionary<int, List<List<int>>>(){
        {0,Mapping(1,4) },
        {1,Mapping(5,8) },
        {2,Mapping(9,12) },
        {3,Mapping(13,16) },
        {4,Mapping(17,20)},
    };

    private static List<List<int>> Mapping(int start, int finish)
    {
        List<List<int>> temp = new List<List<int>>();
        temp.Add(new List<int> { 0, start });
        for (int i = start; i < finish; i++)
        {
            temp.Add(new List<int> { i, i + 1 });
        }
        return temp;
    }

    public static string getName(int index)
    {
        return nameMapping[index];
    }
    public static List<List<int>> getPoints(int index)
    {
        return pointsMapping[index];
    }
}

public class FingerCurl
{
    public static int NoCurl = 0;
    public static int HalfCurl = 1;
    public static int FullCurl = 2;
    static Dictionary<string, int> names = new Dictionary<string, int>(){
        {"NoCurl",  0 },
        {"HalfCurl",  1 },
        {"FullCurl",  2 },
    };

    static Dictionary<int, string> nameMapping = new Dictionary<int, string>(){
        {0,"NoCurl" },
        {1,"HalfCurl" },
        {2,"FullCurl" },
    };


    public static string getName(int index)
    {
        return nameMapping[index];
    }
}

public class FingerDirection
{
    public static int VerticalUp = 0;
    public static int VerticalDown = 1;
    public static int HorizontalLeft = 2;
    public static int HorizontalRight = 3;
    public static int DiagonalUpRight = 4;
    public static int DiagonalUpLeft = 5;
    public static int DiagonalDownRight = 6;
    public static int DiagonalDownLeft = 7;
    static Dictionary<string, int> names = new Dictionary<string, int>(){
        {"VerticalUp",  0 },
        {"VerticalDown",  1 },
        {"HorizontalLeft",  2 },
        {"HorizontalRight",  3 },
        {"DiagonalUpRight",  4 },
        {"DiagonalUpLeft",  5 },
        {"DiagonalDownRight",  6 },
        {"DiagonalDownLeft",  7 },
    };

    static Dictionary<int, string> nameMapping = new Dictionary<int, string>(){
        {0,"VerticalUp" },
        {1,"VerticalDown" },
        {2,"HorizontalLeft" },
        {3,"HorizontalRight" },
        {4,"DiagonalUpRight" },
        {5,"DiagonalUpLeft" },
        {6,"DiagonalDownRight"},
        {7,"DiagonalDownLeft" },
    };


    public static string getName(int index)
    {
        return nameMapping[index];
    }
}
public class GestureDescription
{
    public string name;
    public Dictionary<int, List<Contrib>> curls = new Dictionary<int, List<Contrib>>();
    public Dictionary<int, List<Contrib>> directions = new Dictionary<int, List<Contrib>>();

    public void addCurl(int finger, int curl, float contrib = 1)
    {
        List<Contrib> temp;
        if (this.curls.TryGetValue(finger, out temp))
        {
            this.curls[finger].Add(new Contrib() { index = curl, contrib = contrib });
        }
        else
        {
            this.curls[finger] = new List<Contrib>();
            this.curls[finger].Add(new Contrib() { index = curl, contrib = contrib });
        }
        //new Contrib() { index= curl, contrib = contrib };
    }
    public void addDirection(int finger, int position, float contrib = 1)
    {
        List<Contrib> temp;
        if (this.directions.TryGetValue(finger, out temp))
        {
            this.directions[finger].Add(new Contrib() { index = position, contrib = contrib });
        }
        else
        {
            this.directions[finger] = new List<Contrib>();
            this.directions[finger].Add(new Contrib() { index = position, contrib = contrib });
        }
        //this.directions[finger] = new Contrib() { index = position, contrib = contrib };
    }

    public float matchAgainst(Dictionary<int, Contrib> detectedCurls, Dictionary<int, Contrib> detectedDirections)
    {

        var score = 0.0f;
        var numParameters = 0;

        // look at the detected curl of each finger and compare with
        // the expected curl of this finger inside current gesture
        foreach (var fingerIdx in detectedCurls)
        {

            var detectedCurl = detectedCurls[fingerIdx.Key];
            List<Contrib> expectedCurlslist;
            if (!curls.TryGetValue(fingerIdx.Key, out expectedCurlslist))
            {
                continue;
            }




            // increase the number of relevant parameters
            numParameters++;

            // compare to each possible curl of this specific finger
            bool matchingCurlFound = false;
            float highestCurlContrib = 0;
            foreach (Contrib expectedCurls in expectedCurlslist)
            {
                if (detectedCurl.index == expectedCurls.index)
                {
                    score += expectedCurls.contrib;
                    highestCurlContrib = Mathf.Max(highestCurlContrib, expectedCurls.contrib);
                    matchingCurlFound = true;

                }

                // subtract penalty if curl was expected but not found
                if (!matchingCurlFound)
                {
                    //  score -= highestCurlContrib;
                }
            }
        }

        // same for detected direction of each finger
        foreach (var fingerIdx in detectedDirections)
        {

            var detectedDirection = detectedDirections[fingerIdx.Key];
            List<Contrib> expectedDirectionslis;
            if (!directions.TryGetValue(fingerIdx.Key, out expectedDirectionslis))
            {
                continue;
            }
            // var expectedDirections = this.directions[fingerIdx.Key];



            // increase the number of relevant parameters
            numParameters++;

            // compare to each possible direction of this specific finger
            bool matchingDirectionFound = false;
            float highestDirectionContrib = 0;
            foreach (Contrib expectedDirections in expectedDirectionslis)
            {
                if (detectedDirection.index == expectedDirections.index)
                {
                    score += expectedDirections.contrib;
                    highestDirectionContrib = Mathf.Max(highestDirectionContrib, expectedDirections.contrib);
                    matchingDirectionFound = true;

                }

                // subtract penalty if direction was expected but not found
                if (!matchingDirectionFound)
                {
                    // score -= highestDirectionContrib;
                }
            }
        }

        // multiply final score with 10 (to maintain compatibility)
        float finalScore = (score) * 10;

        return finalScore;
    }
}
public struct Contrib
{
    public int index;
    public float contrib;
}

public class FingerPoseEstimator
{
    public class Options
    {
        // curl estimation
        public float HALF_CURL_START_LIMIT = 60;
        public float NO_CURL_START_LIMIT = 130;

        // direction estimation
        public float DISTANCE_VOTE_POWER = 1.1f;
        public float SINGLE_ANGLE_VOTE_POWER = 0.9f;
        public float TOTAL_ANGLE_VOTE_POWER = 1.6f;
    }
    public Options options;

    public FingerPoseEstimator(Options option)
    {
        this.options = option;
    }
    public EST estimate(HandLandmarkDetect.Result landmarks)
    {
        var slopesXY = new List<List<float>>();
        var slopesYZ = new List<List<float>>();

        foreach (var finger in Finger.all)
        {

            var points = Finger.getPoints(finger);
            var slopeAtXY = new List<float>();
            var slopeAtYZ = new List<float>();

            foreach (var point in points)
            {

                var point1 = landmarks.joints[point[0]];
                var point2 = landmarks.joints[point[1]];

                // calculate single slope
                var slopes = this.getSlopes(point1, point2);
                var slopeXY = slopes[0];
                var slopeYZ = slopes[1];
                slopeAtXY.Add(slopeXY);
                slopeAtYZ.Add(slopeYZ);
            }

            slopesXY.Add(slopeAtXY);
            slopesYZ.Add(slopeAtYZ);
        }

        // step 2: calculate orientations

        Dictionary<int, Contrib> fingerCurls = new Dictionary<int, Contrib>();
        Dictionary<int, Contrib> fingerDirections = new Dictionary<int, Contrib>();

        foreach (var finger in Finger.all)
        {

            // start finger predictions from palm - except for thumb
            var pointIndexAt = (finger == Finger.Thumb) ? 1 : 0;

            var fingerPointsAt = Finger.getPoints(finger);
            var startPoint = landmarks.joints[fingerPointsAt[pointIndexAt][0]];
            var midPoint = landmarks.joints[fingerPointsAt[pointIndexAt + 1][1]];
            var endPoint = landmarks.joints[fingerPointsAt[3][1]];

            // check if finger is curled
            var fingerCurled = this.estimateFingerCurl(
              startPoint, midPoint, endPoint
            );

            var fingerPosition = this.calculateFingerDirection(
              startPoint, midPoint, endPoint,
              slopesXY[finger].GetRange(pointIndexAt, slopesXY[finger].Count - pointIndexAt)
            );

            fingerCurls[finger] = new Contrib { index = fingerCurled };
            fingerDirections[finger] = new Contrib { index = fingerPosition };
        }

        return new EST { curls = fingerCurls, directions = fingerDirections };
    }

    List<float> getSlopes(Vector3 point1, Vector3 point2)
    {

        float slopeXY = this.calculateSlope(new Vector2(point1.x, point1.y), new Vector2(point2.x, point2.y));


        float slopeYZ = this.calculateSlope(new Vector2(point1.y, point1.z), new Vector2(point2.y, point2.z));

        return (new List<float> { slopeXY, slopeYZ });
    }

    float calculateSlope(Vector2 point1, Vector2 point2)
    {

        var value = (point1.y - point2.y) / (point1.x - point2.x);
        var slope = Mathf.Atan(value) * 180 / Mathf.PI;

        if (slope <= 0)
        {
            slope = -slope;
        }
        else if (slope > 0)
        {
            slope = 180 - slope;
        }

        return slope;
    }

    int estimateFingerCurl(Vector3 startPoint, Vector3 midPoint, Vector3 endPoint)
    {

        var start_mid_x_dist = startPoint[0] - midPoint[0];
        var start_end_x_dist = startPoint[0] - endPoint[0];
        var mid_end_x_dist = midPoint[0] - endPoint[0];

        var start_mid_y_dist = startPoint[1] - midPoint[1];
        var start_end_y_dist = startPoint[1] - endPoint[1];
        var mid_end_y_dist = midPoint[1] - endPoint[1];

        var start_mid_z_dist = startPoint[2] - midPoint[2];
        var start_end_z_dist = startPoint[2] - endPoint[2];
        var mid_end_z_dist = midPoint[2] - endPoint[2];

        var start_mid_dist = Mathf.Sqrt(
          start_mid_x_dist * start_mid_x_dist +
          start_mid_y_dist * start_mid_y_dist +
          start_mid_z_dist * start_mid_z_dist
        );
        var start_end_dist = Mathf.Sqrt(
          start_end_x_dist * start_end_x_dist +
          start_end_y_dist * start_end_y_dist +
          start_end_z_dist * start_end_z_dist
        );
        var mid_end_dist = Mathf.Sqrt(
          mid_end_x_dist * mid_end_x_dist +
          mid_end_y_dist * mid_end_y_dist +
          mid_end_z_dist * mid_end_z_dist
        );

        var cos_in = (
          mid_end_dist * mid_end_dist +
          start_mid_dist * start_mid_dist -
          start_end_dist * start_end_dist
        ) / (2 * mid_end_dist * start_mid_dist);

        if (cos_in > 1.0)
        {
            cos_in = 1.0f;
        }
        else if (cos_in < -1.0)
        {
            cos_in = -1.0f;
        }

        var angleOfCurve = Mathf.Acos(cos_in);
        angleOfCurve = (57.2958f * angleOfCurve) % 180;

        int fingerCurl;
        if (angleOfCurve > this.options.NO_CURL_START_LIMIT)
        {
            fingerCurl = FingerCurl.NoCurl;
        }
        else if (angleOfCurve > this.options.HALF_CURL_START_LIMIT)
        {
            fingerCurl = FingerCurl.HalfCurl;
        }
        else
        {
            fingerCurl = FingerCurl.FullCurl;
        }

        return fingerCurl;
    }


    int estimateHorizontalDirection(float start_end_x_dist, float start_mid_x_dist, float mid_end_x_dist, float max_dist_x)
    {

        int estimatedDirection;
        if (max_dist_x == Mathf.Abs(start_end_x_dist))
        {
            if (start_end_x_dist > 0)
            {
                estimatedDirection = FingerDirection.HorizontalLeft;
            }
            else
            {
                estimatedDirection = FingerDirection.HorizontalRight;
            }
        }
        else if (max_dist_x == Mathf.Abs(start_mid_x_dist))
        {
            if (start_mid_x_dist > 0)
            {
                estimatedDirection = FingerDirection.HorizontalLeft;
            }
            else
            {
                estimatedDirection = FingerDirection.HorizontalRight;
            }
        }
        else
        {
            if (mid_end_x_dist > 0)
            {
                estimatedDirection = FingerDirection.HorizontalLeft;
            }
            else
            {
                estimatedDirection = FingerDirection.HorizontalRight;
            }
        }

        return estimatedDirection;
    }

    int estimateVerticalDirection(float start_end_y_dist, float start_mid_y_dist, float mid_end_y_dist, float max_dist_y)
    {

        int estimatedDirection;
        if (max_dist_y == Mathf.Abs(start_end_y_dist))
        {
            if (start_end_y_dist < 0)
            {
                estimatedDirection = FingerDirection.VerticalDown;
            }
            else
            {
                estimatedDirection = FingerDirection.VerticalUp;
            }
        }
        else if (max_dist_y == Mathf.Abs(start_mid_y_dist))
        {
            if (start_mid_y_dist < 0)
            {
                estimatedDirection = FingerDirection.VerticalDown;
            }
            else
            {
                estimatedDirection = FingerDirection.VerticalUp;
            }
        }
        else
        {
            if (mid_end_y_dist < 0)
            {
                estimatedDirection = FingerDirection.VerticalDown;
            }
            else
            {
                estimatedDirection = FingerDirection.VerticalUp;
            }
        }

        return estimatedDirection;
    }

    int estimateDiagonalDirection(
       float start_end_y_dist, float start_mid_y_dist, float mid_end_y_dist, float max_dist_y,
       float start_end_x_dist, float start_mid_x_dist, float mid_end_x_dist, float max_dist_x

    )
    {

        int estimatedDirection;
        var reqd_vertical_direction = this.estimateVerticalDirection(
          start_end_y_dist, start_mid_y_dist, mid_end_y_dist, max_dist_y
        );
        var reqd_horizontal_direction = this.estimateHorizontalDirection(
          start_end_x_dist, start_mid_x_dist, mid_end_x_dist, max_dist_x
        );

        if (reqd_vertical_direction == FingerDirection.VerticalUp)
        {
            if (reqd_horizontal_direction == FingerDirection.HorizontalLeft)
            {
                estimatedDirection = FingerDirection.DiagonalUpLeft;
            }
            else
            {
                estimatedDirection = FingerDirection.DiagonalUpRight;
            }
        }
        else
        {
            if (reqd_horizontal_direction == FingerDirection.HorizontalLeft)
            {
                estimatedDirection = FingerDirection.DiagonalDownLeft;
            }
            else
            {
                estimatedDirection = FingerDirection.DiagonalDownRight;
            }
        }

        return estimatedDirection;
    }

    int calculateFingerDirection(Vector3 startPoint, Vector3 midPoint, Vector3 endPoint, List<float> fingerSlopes)
    {

        var start_mid_x_dist = startPoint[0] - midPoint[0];
        var start_end_x_dist = startPoint[0] - endPoint[0];
        var mid_end_x_dist = midPoint[0] - endPoint[0];

        var start_mid_y_dist = startPoint[1] - midPoint[1];
        var start_end_y_dist = startPoint[1] - endPoint[1];
        var mid_end_y_dist = midPoint[1] - endPoint[1];

        var max_dist_x = Mathf.Max(
          Mathf.Abs(start_mid_x_dist),
          Mathf.Abs(start_end_x_dist),
           Mathf.Abs(mid_end_x_dist)
        );
        var max_dist_y = Mathf.Max(
          Mathf.Abs(start_mid_y_dist),
           Mathf.Abs(start_end_y_dist),
          Mathf.Abs(mid_end_y_dist)
        );

        var voteVertical = 0.0f;
        var voteDiagonal = 0.0f;
        var voteHorizontal = 0.0f;

        var start_end_x_y_dist_ratio = max_dist_y / (max_dist_x + 0.00001);
        if (start_end_x_y_dist_ratio > 1.5)
        {
            voteVertical += this.options.DISTANCE_VOTE_POWER;
        }
        else if (start_end_x_y_dist_ratio > 0.66)
        {
            voteDiagonal += this.options.DISTANCE_VOTE_POWER;
        }
        else
        {
            voteHorizontal += this.options.DISTANCE_VOTE_POWER;
        }

        var start_mid_dist = Mathf.Sqrt(
          start_mid_x_dist * start_mid_x_dist + start_mid_y_dist * start_mid_y_dist
        );
        var start_end_dist = Mathf.Sqrt(
          start_end_x_dist * start_end_x_dist + start_end_y_dist * start_end_y_dist
        );
        var mid_end_dist = Mathf.Sqrt(
          mid_end_x_dist * mid_end_x_dist + mid_end_y_dist * mid_end_y_dist
        );

        var max_dist = Mathf.Max(start_mid_dist, start_end_dist, mid_end_dist);
        float calc_start_point_x = startPoint.x,
            calc_start_point_y = startPoint[1];
        float calc_end_point_x = endPoint[0],
            calc_end_point_y = endPoint[1];

        if (max_dist == start_mid_dist)
        {
            calc_end_point_x = endPoint[0];
            calc_end_point_y = endPoint[1];
        }
        else if (max_dist == mid_end_dist)
        {
            calc_start_point_x = midPoint[0];
            calc_start_point_y = midPoint[1];
        }

        var calcStartPoint = new Vector3(calc_start_point_x, calc_start_point_y);
        var calcEndPoint = new Vector3(calc_end_point_x, calc_end_point_y);

        var totalAngle = this.getSlopes(calcStartPoint, calcEndPoint);
        var votes = this.angleOrientationAt(totalAngle[0], this.options.TOTAL_ANGLE_VOTE_POWER);
        voteVertical += votes[0];
        voteDiagonal += votes[1];
        voteHorizontal += votes[2];

        foreach (var fingerSlope in fingerSlopes)
        {
            var votes1 = this.angleOrientationAt(fingerSlope, this.options.SINGLE_ANGLE_VOTE_POWER);
            voteVertical += votes1[0];
            voteDiagonal += votes1[1];
            voteHorizontal += votes1[2];
        }

        // in case of tie, highest preference goes to Vertical,
        // followed by horizontal and then diagonal
        int estimatedDirection;
        if (voteVertical == Mathf.Max(voteVertical, voteDiagonal, voteHorizontal))
        {
            estimatedDirection = this.estimateVerticalDirection(
              start_end_y_dist,
              start_mid_y_dist,
              mid_end_y_dist, max_dist_y
            );
        }
        else if (voteHorizontal == Mathf.Max(voteDiagonal, voteHorizontal))
        {
            estimatedDirection = this.estimateHorizontalDirection(
              start_end_x_dist,
              start_mid_x_dist,
              mid_end_x_dist, max_dist_x
            );
        }
        else
        {
            estimatedDirection = this.estimateDiagonalDirection(
              start_end_y_dist, start_mid_y_dist,
              mid_end_y_dist, max_dist_y,
              start_end_x_dist, start_mid_x_dist,
              mid_end_x_dist, max_dist_x
            );
        }

        return chanDirge(estimatedDirection);
    }
    List<float> angleOrientationAt(float angle, float weightageAt = 1.0f)
    {

        var isVertical = 0f;
        var isDiagonal = 0f;
        var isHorizontal = 0f;

        if (angle >= 75.0 && angle <= 105.0)
        {
            isVertical = 1 * weightageAt;
        }
        else if (angle >= 25.0 && angle <= 155.0)
        {
            isDiagonal = 1 * weightageAt;
        }
        else
        {
            isHorizontal = 1 * weightageAt;
        }

        return new List<float> { isVertical, isDiagonal, isHorizontal };
    }
    int chanDirge(int dir)
    {
        if (dir == FingerDirection.VerticalUp)
        {
            return FingerDirection.HorizontalRight;
        }
        else if (dir == FingerDirection.VerticalDown)
        {
            return FingerDirection.HorizontalLeft;
        }
        else if (dir == FingerDirection.DiagonalDownRight)
        {
            return FingerDirection.DiagonalUpLeft;
        }
        else if (dir == FingerDirection.DiagonalUpLeft)
        {
            return FingerDirection.DiagonalDownRight;
        }
        else if (dir == FingerDirection.HorizontalLeft)
        {
            return FingerDirection.VerticalDown;
        }
        else if (dir == FingerDirection.HorizontalRight)
        {
            return FingerDirection.VerticalUp;
        }
        else
        {
            return dir;
        }
    }
}


public class Gesture
{
    public GestureDescription thumbsUpDescription = new GestureDescription();
}

public struct EST
{
    public Dictionary<int, Contrib> curls;
    public Dictionary<int, Contrib> directions;

}
public class GestureEstimator
{
    public class Result
    {
        public List<List<string>> poseData;
        public List<Dictionary<string, float>> gestures;
        public string resultStr;
        public string maxGesture;
        public float maxScore;
        public Result(List<List<string>> poseData, List<Dictionary<string, float>> gestures, string resultStr, string maxGesture,float maxScore)
        {
            this.gestures = gestures;
            this.poseData = poseData;
            this.resultStr = resultStr;
            this.maxGesture = maxGesture;
            this.maxScore = maxScore;
        }
    }
    Gesture[] gestures;
    FingerPoseEstimator estimator;
    string resultStr = "";
    public GestureEstimator(Gesture[] knownGestures, FingerPoseEstimator.Options estimatorOptions)
    {
        this.estimator = new FingerPoseEstimator(estimatorOptions);

        // list of predefined gestures
        this.gestures = knownGestures;
    }

    public Result estimate(HandLandmarkDetect.Result landmarks, float minScore)
    {
        resultStr = "";
        var gesturesFound = new List<Dictionary<string, float>>();

        // step 1: get estimations of curl / direction for each finger
        var est = this.estimator.estimate(landmarks);

        var poseData = new List<List<string>>(); ;
        foreach (var fingerIdx in Finger.all)
        {
            resultStr += $"{Finger.getName(fingerIdx)} : {FingerCurl.getName(est.curls[fingerIdx].index)} : {FingerDirection.getName(est.directions[fingerIdx].index)}\n";

            poseData.Add(new List<string>{ Finger.getName(fingerIdx),
          FingerCurl.getName(est.curls[fingerIdx].index),
          FingerDirection.getName(est.directions[fingerIdx].index) }
        );
        }

        // step 2: compare gesture description to each known gesture
        var max = -1f;
        var maxges = "";
        foreach (var gesture in this.gestures)
        {

            var score = gesture.thumbsUpDescription.matchAgainst(est.curls, est.directions);
            resultStr += $"{gesture.thumbsUpDescription.name}:- {score}\n";
         
            if (score >= minScore)
            {
                var ges = new Dictionary<string, float>();
                ges.Add(gesture.thumbsUpDescription.name, score);
                gesturesFound.Add(ges);
                if (max <= score)
                {
                    max = score;
                    maxges = gesture.thumbsUpDescription.name;
                }
            }
        }

        return new Result(poseData, gesturesFound, resultStr, maxges,max);
    }
}
public class ThumsUp : Gesture
{
    public ThumsUp()
    {
        thumbsUpDescription.name = "thumbs_up";
        thumbsUpDescription.addCurl(Finger.Thumb, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1f);

        // all other fingers:
        // - curled (best)
        // - half curled (acceptable)
        // - pointing down is NOT acceptable
        foreach (var finger in new List<int> { Finger.Index, Finger.Middle, Finger.Ring, Finger.Pinky })
        {
            thumbsUpDescription.addCurl(finger, FingerCurl.FullCurl, 2.0f);
            thumbsUpDescription.addCurl(finger, FingerCurl.HalfCurl, 2f);

        }

        // require the index finger to be somewhat left or right pointing
        // but NOT down and NOT fully up
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalRight, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 1.0f);
    }
}

public class Victory : Gesture
{
    public Victory()
    {
        thumbsUpDescription.name = "victory";
        // thumb:
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.FullCurl, 1.0f);
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.HalfCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1.0f);

        // index:
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalRight, 1.0f);

        // middle:
        thumbsUpDescription.addCurl(Finger.Middle, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpRight, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalRight, 1.0f);

        // ring:
        //  thumbsUpDescription.addCurl(Finger.Ring, FingerCurl.FullCurl, 1.0f);
        //  thumbsUpDescription.addCurl(Finger.Ring, FingerCurl.HalfCurl, 0.9f);


        // pinky:
        // thumbsUpDescription.addCurl(Finger.Pinky, FingerCurl.FullCurl, 1.0f);
        // thumbsUpDescription.addCurl(Finger.Pinky, FingerCurl.HalfCurl, 0.9f);

        foreach (var finger in new List<int> { Finger.Ring, Finger.Pinky })
        {
            thumbsUpDescription.addCurl(finger, FingerCurl.FullCurl, 2f);
            thumbsUpDescription.addCurl(finger, FingerCurl.HalfCurl, 2f);
            //   thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalUpLeft, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.HorizontalLeft, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.HorizontalRight, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalUpRight, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalDownRight, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalDownLeft, 1.0f);
            //   thumbsUpDescription.addDirection(finger, FingerDirection.VerticalDown, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.VerticalUp, 1.0f);
        }
    }
}

public class OK : Gesture
{
    public OK()
    {
        thumbsUpDescription.name = "ok";
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.FullCurl, 1.0f);
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.HalfCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1.0f);
        foreach (var finger in new List<int> { Finger.Index })
        {
            thumbsUpDescription.addCurl(finger, FingerCurl.FullCurl, 2f);
            thumbsUpDescription.addCurl(finger, FingerCurl.HalfCurl, 2f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalUpLeft, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.HorizontalLeft, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.HorizontalRight, 1.0f);
            //thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalUpRight, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalDownRight, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalDownLeft, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.VerticalDown, 1.0f);
            //thumbsUpDescription.addDirection(finger, FingerDirection.VerticalUp, 1.0f);
        }
        // thumb:
        // thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.VerticalUp, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1.0f);
        //thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1.0f);

        // index:
        // thumbsUpDescription.addCurl(Finger.Index, FingerCurl.FullCurl, 1.0f);
        // thumbsUpDescription.addCurl(Finger.Index, FingerCurl.HalfCurl, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.VerticalUp, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalRight, 1.0f);

        // middle:
        thumbsUpDescription.addCurl(Finger.Middle, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpRight, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalRight, 1.0f);

        // ring:
        thumbsUpDescription.addCurl(Finger.Ring, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.DiagonalUpRight, 1.0f);
        //   thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.HorizontalLeft, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.HorizontalRight, 1.0f);

        // pinky:
        thumbsUpDescription.addCurl(Finger.Pinky, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.DiagonalUpRight, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.HorizontalRight, 1.0f);
    }


}

public class Five : Gesture
{
    public Five()
    {
        thumbsUpDescription.name = "five";
        // thumb:
        thumbsUpDescription.addCurl(Finger.Thumb, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1.0f);

        // index:
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalRight, 1.0f);

        // middle:
        thumbsUpDescription.addCurl(Finger.Middle, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpRight, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalRight, 1.0f);

        // ring:
        thumbsUpDescription.addCurl(Finger.Ring, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.DiagonalUpRight, 1.0f);
        //   thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.HorizontalLeft, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.HorizontalRight, 1.0f);

        // pinky:
        thumbsUpDescription.addCurl(Finger.Pinky, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.DiagonalUpRight, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.HorizontalRight, 1.0f);
    }


}

public class LoveYou : Gesture
{
    public LoveYou()
    {
        thumbsUpDescription.name = "love_you";
        // thumb:
        thumbsUpDescription.addCurl(Finger.Thumb, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.HorizontalLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.HorizontalRight, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1.0f);
        thumbsUpDescription.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1.0f);

        // index:
        thumbsUpDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Index, FingerDirection.HorizontalRight, 1.0f);

        // middle:
        //thumbsUpDescription.addCurl(Finger.Middle, FingerCurl.FullCurl, 1.0f);
        // thumbsUpDescription.addCurl(Finger.Middle, FingerCurl.HalfCurl, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.DiagonalUpRight, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Middle, FingerDirection.HorizontalRight, 1.0f);

        // ring:
        //thumbsUpDescription.addCurl(Finger.Ring, FingerCurl.FullCurl, 1.0f);
        // thumbsUpDescription.addCurl(Finger.Ring, FingerCurl.HalfCurl, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.VerticalUp, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.DiagonalUpLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.DiagonalUpRight, 1.0f);
        //   thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.HorizontalLeft, 1.0f);
        //  thumbsUpDescription.addDirection(Finger.Ring, FingerDirection.HorizontalRight, 1.0f);

        // pinky:
        thumbsUpDescription.addCurl(Finger.Pinky, FingerCurl.NoCurl, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.VerticalUp, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.DiagonalUpLeft, 1.0f);
        thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.DiagonalUpRight, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.HorizontalLeft, 1.0f);
        // thumbsUpDescription.addDirection(Finger.Pinky, FingerDirection.HorizontalRight, 1.0f);

        foreach (var finger in new List<int> { Finger.Ring, Finger.Middle })
        {
            thumbsUpDescription.addCurl(finger, FingerCurl.FullCurl, 2f);
            thumbsUpDescription.addCurl(finger, FingerCurl.HalfCurl, 2f);
            //thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalUpLeft, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.HorizontalLeft, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.HorizontalRight, 1.0f);
            // thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalUpRight, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalDownRight, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.DiagonalDownLeft, 1.0f);
            //  thumbsUpDescription.addDirection(finger, FingerDirection.VerticalDown, 1.0f);
            //thumbsUpDescription.addDirection(finger, FingerDirection.VerticalUp, 1.0f);
        }
    }


}